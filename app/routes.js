// load the todos model
var Todo = require('./models/todo');

// expose the routes to the app with module.exports
module.exports = function(app) {
    // get all todos
    app.get('/api/todos', function(req, res) {

        // use mongoose to get all todos in the database
        Todo.find(function(err, todos) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                res.send(err)

            res.json(todos); // return all todos in JSON format
        });
    });

    // create todos and send back all todos after creation
    app.post('/api/todos', function(req, res) {

        // create todos, information comes from AJAX request from Angular
        Todo.create({
            text : req.body.text,
            complete : false
        }, function(err, todo) {
            if (err)
                res.send(err);

            // get and return all the todos after you create another
            Todo.find(function(err, todos) {
                if (err)
                    res.send(err)
                res.json(todos);
            });
        });

    });

    // update todos if it is checked
    app.post('/api/todos_checked/:todo_id', function(req, res) {

        // create todos, information comes from AJAX request from Angular
        Todo.update({
            _id : req.params.todo_id,
            complete : true
        }, function(err, todo) {
            if (err)
                res.send(err);

            // get and return all the todos after you create another
            Todo.find(function(err, todos) {
                if (err)
                    res.send(err)
                res.json(todos);
            });
        });

    });

    // delete a todos
    app.delete('/api/todos/:todo_id', function(req, res) {
        Todo.remove({
            _id : req.params.todo_id
        }, function(err, todo) {
            if (err)
                res.send(err);

            // get and return all the todos after you create another
            Todo.find(function(err, todos) {
                if (err)
                    res.send(err)
                res.json(todos);
            });
        });
    });
}